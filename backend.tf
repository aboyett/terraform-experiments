terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "agb"

    workspaces {
      name = "agb-io"
    }
  }
}
