resource "digitalocean_tag" "k3s" {
  name = "k3s"
}

resource "digitalocean_tag" "k3s_server" {
  name = "k3s-server"
}