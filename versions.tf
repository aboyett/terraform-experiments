
terraform {
  required_version = ">= 0.13"
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.4.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 1.9"
    }
  }
}
