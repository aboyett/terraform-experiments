resource "digitalocean_vpc" "agb_io_services" {
  name     = "agb.io-services"
  region   = "sfo2"
  ip_range = "172.16.0.0/16"
}

resource "digitalocean_firewall" "k3s-admin" {
  name = "k3s-admin"

  tags = [digitalocean_tag.k3s_server.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "6443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "30667"
    source_load_balancer_uids = [ digitalocean_loadbalancer.k3s_ingress_lb.id ]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "30688"
    source_load_balancer_uids = [ digitalocean_loadbalancer.k3s_ingress_lb.id ]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "31300"
    source_load_balancer_uids = [ digitalocean_loadbalancer.k3s_ingress_lb.id ]
  }
}

resource "digitalocean_loadbalancer" "k3s_ingress_lb" {
  name   = "k3s-ingress-lb"
  region = digitalocean_vpc.agb_io_services.region
  vpc_uuid = digitalocean_vpc.agb_io_services.id

  algorithm = "least_connections"

  droplet_tag = digitalocean_tag.k3s_server.id

  forwarding_rule  {
    entry_port = 80
    entry_protocol = "http"
    target_port = 30688
    target_protocol = "http"
  }

  forwarding_rule  {
    entry_port = 443
    entry_protocol = "https"
    target_port = 31300
    target_protocol = "https"
    tls_passthrough = true
  }

  forwarding_rule  {
    entry_port = 6667
    entry_protocol = "tcp"
    target_port = 30667
    target_protocol = "tcp"
  }
}
