/*
resource "digitalocean_kubernetes_cluster" "beta-k8s-agb-io" {
  name    = "beta-k8s-agb-io"
  region  = "sfo2"
  version = "1.16.2-do.0"

  node_pool {
    name       = "beta-mini-nodes"
    node_count = 2
    size       = "s-2vcpu-4gb"
    tags       = ["beta", "small"]
  }
}

provider "kubernetes" {
  alias = "k8s"
  host  = digitalocean_kubernetes_cluster.beta-k8s-agb-io.endpoint

  client_certificate = base64decode(
    digitalocean_kubernetes_cluster.beta-k8s-agb-io.kube_config[0].client_certificate,
  )
  client_key = base64decode(
    digitalocean_kubernetes_cluster.beta-k8s-agb-io.kube_config[0].client_key,
  )
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.beta-k8s-agb-io.kube_config[0].cluster_ca_certificate,
  )
}
*/
