# domains
resource "digitalocean_domain" "agb_io" {
  name = "agb.io"
}

resource "digitalocean_domain" "do_agb_io" {
  name = "do.agb.io"
}

resource "digitalocean_domain" "notfunny_lol" {
  name = "notfunny.lol"
}

# agb.io records
resource "digitalocean_record" "irc_agb_io_A" {
  domain = digitalocean_domain.agb_io.name
  type   = "A"
  name   = "irc"
  value  = digitalocean_droplet.irc-01.ipv4_address
}

resource "digitalocean_record" "agb_io_NS" {
  count  = 3
  domain = digitalocean_domain.agb_io.name
  type   = "NS"
  name   = "@"
  value  = "ns${count.index + 1}.digitalocean.com."
  ttl    = 1800
}

resource "digitalocean_record" "agb_io_NS_do" {
  count  = 3
  domain = digitalocean_domain.agb_io.name
  type   = "NS"
  name   = "do"
  value  = "ns${count.index + 1}.digitalocean.com."
  ttl    = 1800
}

resource "digitalocean_record" "agb_io_TXT_protonmail" {
  domain = digitalocean_domain.agb_io.name
  type   = "TXT"
  name   = "@"
  value  = "protonmail-verification=072c7a8606540b6c0e249dfd24414432488c5f21"
  ttl    = 3600
}

resource "digitalocean_record" "agb_io_MX_10" {
  domain   = digitalocean_domain.agb_io.name
  type     = "MX"
  name     = "@"
  priority = 10
  value    = "mail.protonmail.ch."
  ttl      = 14400
}

resource "digitalocean_record" "agb_io_TXT_spf" {
  domain = digitalocean_domain.agb_io.name
  type   = "TXT"
  name   = "@"
  value  = "v=spf1 include:_spf.protonmail.ch mx ~all"
  ttl    = 3600
}

resource "digitalocean_record" "dmarc_agb_io_TXT_dmarc" {
  domain = digitalocean_domain.agb_io.name
  type   = "TXT"
  name   = "_dmarc"
  value  = "v=DMARC1; p=none; rua=mailto:webmaster@agb.io"
  ttl    = 3600
}

resource "digitalocean_record" "protonmail_domainkey_agb_io_TXT_dkim" {
  domain = digitalocean_domain.agb_io.name
  type   = "TXT"
  name   = "protonmail._domainkey"
  value  = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDLMRpAW/EvGmqBn62lBSvfLxmY5SBXHJvIlJiRKQrXo2CgLHed3dKOmlFEZ9bDbysgsmYy3iFTDGew3m/PNibv+EkiOgz9EfWj4c8m1HfKgxceQk0PntrXFqh2fIxRvuSmMIZPfl0G3ddu1lg/X47JqNXn+7ajSungHxP+y126MwIDAQAB"
  ttl    = 3600
}

# do.agb.io records
resource "digitalocean_record" "do_agb_io_NS" {
  count  = 3
  domain = digitalocean_domain.do_agb_io.name
  type   = "NS"
  name   = "@"
  value  = "ns${count.index + 1}.digitalocean.com."
  ttl    = 1800
}

resource "digitalocean_record" "k3s_do_agb_io_A" {
  domain = digitalocean_domain.do_agb_io.name
  type   = "A"
  name   = "k3s"
  value  = digitalocean_droplet.k3s_server_01.ipv4_address
  ttl    = 180
}

# notfunny.lol records
resource "digitalocean_record" "notfunny_lol_NS" {
  count  = 3
  domain = digitalocean_domain.notfunny_lol.name
  type   = "NS"
  name   = "@"
  value  = "ns${count.index + 1}.digitalocean.com."
  ttl    = 1800
}

resource "digitalocean_record" "you_are_notfunny_lol_A" {
  domain = digitalocean_domain.notfunny_lol.name
  type   = "A"
  name   = "you.are"
  value  = digitalocean_droplet.botserver.ipv4_address
  ttl    = 1800
}

