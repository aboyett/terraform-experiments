resource "digitalocean_droplet" "irc-01" {
  name  = "irc-01"
  image = "24725963" # debian-8.8-x64

  #image = "debian-9-x64"

  region     = "sfo2"
  size       = "s-1vcpu-1gb"
  tags       = ["irc"]
  backups    = true
  monitoring = true
}

resource "digitalocean_droplet" "botserver" {
  name  = "you.are.notfunny.lol"
  image = "33149562" # CoreOS 1688.5.3 (stable)

  #image = coreos-stable

  private_networking = true
  region             = "sfo2"
  size               = "s-1vcpu-1gb"
  tags               = ["irc"]
}

resource "digitalocean_droplet" "k3s_server_01" {
  name  = "k3s-server-01"
  image = "debian-10-x64"

  region             = digitalocean_vpc.agb_io_services.region
  size               = "s-2vcpu-4gb"
  ssh_keys           = [data.digitalocean_ssh_key.andy_navi_2019_04.fingerprint]
  private_networking = true
  vpc_uuid           = digitalocean_vpc.agb_io_services.id
  tags               = [digitalocean_tag.k3s.id, digitalocean_tag.k3s_server.id]

  backups    = true
  monitoring = true
}
